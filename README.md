![JESYMCA](http://www.jesuministrosymas.com.ve/LOGO.png)

# Kernel Curso Aprende a Fabricar Productos de Limpieza

![Productos de Limpieza](http://www.jesuministrosymas.com.ve/_/rsrc/1501448128002/productos/cursos/elabore-productos-de-limpieza/VENTANA_SEGURIDAD_3.png?height=226&width=320)

El o los archivos acá publicados sirven de seguimiento a las actualizaciones constantes de nuestro Kernel o nucleo central de funcionamiento.

Si desea mas información puede hacerlo ingresando a nuestra web:

[IR A MAS DETALLES SOBRE PRODUCTOS DE LIMPIEZA](http://www.jesuministrosymas.com.ve/productos/cursos/elabore-productos-de-limpieza)

## Aplicativo
El aplicativo que ofrecemos permite efectuar cálculos de manera automática de los ingredientes que se necesitan para la fabricación de mas de 100 productos de limpieza.
Nuestro aplicativo funciona en base a un conjunto de operadores de variables que corren en ambiente JavaScript y que hacen posible que pueda ejecutarse en casi cualquier dispositivo que disponga de un navegador web instalado.

## Como Actualizar

La estructura de nuestro aplicativo es el siguiente:

:file_folder: extra

:file_folder: pag

:file_folder: media

:spiral_notepad: index.html


el kernel o nucleo actualizable se encuentra en la ruta:

:file_folder: pag/:file_folder: librerias/:spiral_notepad: kernel.js

/pag/librerias/kernel.js

esta es la ruta que usted debe seguir para sustituir un kernel viejo por uno nuevo, solo con esta acción de sustitución su aplicativo estará 100% actualizado.

## Como es el Funcionamiento
Es sencillo solo debe seleccionar cualquiera de los módulos disponibles y automáticamente se cargara las opciones que le permitirán interactuar con el sistema, si va a efectuar cálculos de formulas de los productos de limpieza solo debe seleccionar el producto que desea preparar, colocar la cantidad y automáticamente el sistema efectuara el calculo y le dará las instrucciones a seguir para preparar dicho producto.

![Como funciona 1](http://www.jesuministrosymas.com.ve/_/rsrc/1598734866590/productos/cursos/elabore-productos-de-limpieza/Captura%20de%20pantalla_2020-08-24_11-22-46.png)

![Como funciona 2](http://www.jesuministrosymas.com.ve/_/rsrc/1598734870459/productos/cursos/elabore-productos-de-limpieza/Captura%20de%20pantalla_2020-08-24_11-23-25.png)



_J.E Suministros y Mas, C.A._

[Nuestra Web](http://www.jesuministrosymas.com.ve/)

[Canal de Telegram](https://t.me/jesuministrosymas_canal)

[WhatsApp](http://bit.ly/ProductosLimpiezaGithub)
